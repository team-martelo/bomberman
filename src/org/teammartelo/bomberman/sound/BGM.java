package org.teammartelo.bomberman.sound;

import org.teammartelo.bomberman.objects.Bomb;

public class BGM {

    // fields
    private final String[] soundNames;
    private Sound bgm;
    public boolean playing;

    // constructor
    public BGM(){
        this.soundNames = new String[3];
        this.playing = true;
        playSound();
    }

    public void playSound(){
        this.bgm = new Sound("/resources/bmanSound/mxLoop1.wav");
        bgm.setLoop(-1);
    }

    public void mute() {
      if(!Bomb.stopMotherFucker) {
          if (playing) {
              this.playing = false;
              this.bgm.stop();
          } else {
              this.playing = true;
              playSound();
          }
      }
    }

    public void close(){
        this.bgm.close();
    }
}
