package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.objects.ExplodableWall;
import org.teammartelo.bomberman.objects.PathWall;
import javax.swing.*;
import java.util.ArrayList;

public class Field extends JFrame {

    // fields
    public static final int PADDING = 74;
    public static final int PADDING2 = 10;
    public static final int CELLSIZE = 64;
    public static final int MATRIX_SIZE = 12;
    public static final int WIDTH = (MATRIX_SIZE - 1) * CELLSIZE;
    public static final int HEIGHT = (MATRIX_SIZE - 1) * CELLSIZE;
    public static PathWall[] pathWallField;
    public static ArrayList<ExplodableWall> explodableWallField;

    // constructor
    public Field() {
        pathWallField = new PathWall[25];
        explodableWallField = new ArrayList<>();
        drawGreenField(MATRIX_SIZE);
        drawPathWall();
    }

    public void drawGreenField(int matrixSize) {
        Picture desenhaParaAi3 = new Picture(PADDING2,  PADDING2, "resources/field/aveMaria.png");
        desenhaParaAi3.draw();

        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                if (i < matrixSize - 1 && j < matrixSize - 1) {
                    Picture greenBlock = new Picture((PADDING + CELLSIZE * (j)),
                            (PADDING + CELLSIZE * i),
                            "resources/field/BackgroundTile.png");
                    greenBlock.draw();
                } else {
                    Picture rectangle = new Picture(PADDING + CELLSIZE * j,
                            PADDING + CELLSIZE * i, "resources/field/aveMaria.png");
                    rectangle.draw();
                }
                Picture desenhaParaAi = new Picture(PADDING2 + CELLSIZE * (j+1), PADDING2 ,
                        "resources/field/aveMaria.png");
                desenhaParaAi.draw();
                Picture desenhaParaAi2 = new Picture(PADDING2,  PADDING2 + (CELLSIZE * (j + 1)),
                        "resources/field/aveMaria.png");
                desenhaParaAi2.draw();
            }
        }
    }

    public void drawPathWall() {

        int counter = 0;
        int counter2 = 0;

        for (int i = 0; i < (MATRIX_SIZE - 1); i++) {
            for (int j = 0; j < (MATRIX_SIZE - 1); j++) {
                if (i % 2 != 0 && j % 2 != 0) {
                    PathWall pathWall = new PathWall((PADDING + CELLSIZE * j), (PADDING + CELLSIZE * i));
                    this.pathWallField[counter] = pathWall;
                    counter++;
                } else {
                    if ((PADDING + CELLSIZE * i >= 150 || PADDING + CELLSIZE *j >= 150)  && (PADDING + CELLSIZE * i <= 600 || PADDING + CELLSIZE *j <= 600)) {
                        if(Math.random() > 0.40f) {
                            ExplodableWall explodableWall = new ExplodableWall((PADDING + CELLSIZE * j), (PADDING + CELLSIZE * i));
                            explodableWallField.add(counter2, explodableWall);
                            explodableWall.draw();
                        }
                    }
                }
            }
        }
    }
}
