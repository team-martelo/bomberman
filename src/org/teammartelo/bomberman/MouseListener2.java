package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;

public class MouseListener2 implements MouseHandler {

    // fields
    private Menu menu;
    private boolean mouseListenerIsActive;

    // constructors
    public MouseListener2(Menu menu) {
        this.menu = menu;
        setupMouse2();
        mouseListenerIsActive = true;
    }

    public void setupMouse2() {
        Mouse mouse2 = new Mouse(this);
        MouseEvent click2 = new MouseEvent(Field.WIDTH, Field.HEIGHT, MouseEventType.MOUSE_CLICKED);
        mouse2.addEventListener(MouseEventType.MOUSE_CLICKED);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseListenerIsActive) {
            if (mouseEvent.getEventType() == MouseEventType.MOUSE_CLICKED) {
                this.menu.showGame();

                Menu.menuMx.close();
                mouseListenerIsActive = false;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent var1) {
    }
}
