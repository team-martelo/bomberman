package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.teammartelo.bomberman.sound.BGM;

import java.io.IOException;
import java.net.URISyntaxException;

public class KeyboardListener implements KeyboardHandler {

    // fields
    private final Bomberman player;
    private final BGM bgm;
    private Menu menu;

    // constructor
    public KeyboardListener(Bomberman player, BGM bgm, Menu menu) {
        this.player = player;
        this.bgm = bgm;
        this.menu = menu;
        this.setup();
    }

    public void setup() {

        Keyboard keyboard = new Keyboard(this);

        KeyboardEvent rightArrow = new KeyboardEvent();
        rightArrow.setKey(KeyboardEvent.KEY_RIGHT);
        rightArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rightArrow);

        KeyboardEvent leftArrow = new KeyboardEvent();
        leftArrow.setKey(KeyboardEvent.KEY_LEFT);
        leftArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(leftArrow);

        KeyboardEvent upArrow = new KeyboardEvent();
        upArrow.setKey(KeyboardEvent.KEY_UP);
        upArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(upArrow);

        KeyboardEvent downArrow = new KeyboardEvent();
        downArrow.setKey(KeyboardEvent.KEY_DOWN);
        downArrow.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(downArrow);

        KeyboardEvent space = new KeyboardEvent();
        space.setKey(KeyboardEvent.KEY_SPACE);
        space.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(space);

        KeyboardEvent mKey = new KeyboardEvent();
        mKey.setKey(KeyboardEvent.KEY_M);
        mKey.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(mKey);

        KeyboardEvent qKey = new KeyboardEvent();
        qKey.setKey(KeyboardEvent.KEY_Q);
        qKey.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(qKey);

        KeyboardEvent pKey = new KeyboardEvent();
        pKey.setKey(KeyboardEvent.KEY_P);
        pKey.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(pKey);

        KeyboardEvent rKey = new KeyboardEvent();
        rKey.setKey(KeyboardEvent.KEY_R);
        rKey.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(rKey);
    }

    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        if (keyboardEvent.getKey() == KeyboardEvent.KEY_RIGHT) {
            this.player.moveRight();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_LEFT) {
            this.player.moveLeft();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_DOWN) {
            this.player.moveDown();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_UP) {
            this.player.moveUp();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_SPACE) {
            this.player.releaseBomb();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_M) {
            this.bgm.mute();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_Q) {
            System.exit(1);
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_P) {
            this.menu.pause();
        }

        if (keyboardEvent.getKey() == KeyboardEvent.KEY_R) {
            try {
                if (Menu.canRestart) {
                    this.menu.restartApplication();
                }
            } catch (IOException e) {
                System.out.println("fuck this game");
            } catch (URISyntaxException e) {
                System.out.println(".|.");
            }
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}
