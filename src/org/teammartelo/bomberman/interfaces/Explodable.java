package org.teammartelo.bomberman.interfaces;

public interface Explodable {

    boolean isExploded();

    void explode();
}
