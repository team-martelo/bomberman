package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.mouse.Mouse;
import org.academiadecodigo.simplegraphics.mouse.MouseEvent;
import org.academiadecodigo.simplegraphics.mouse.MouseEventType;
import org.academiadecodigo.simplegraphics.mouse.MouseHandler;

public class MouseListener1 implements MouseHandler {

    // fields
    private Menu menu;
    private boolean mouseListenerIsActive;

    // constructor
    public MouseListener1(Menu menu) {
        mouseListenerIsActive = true;
        this.menu = menu;
        setupMouse();
    }

    public void setupMouse() {
        Mouse mouse = new Mouse(this);
        MouseEvent click = new MouseEvent(Field.WIDTH, Field.HEIGHT, MouseEventType.MOUSE_CLICKED);
        mouse.addEventListener(MouseEventType.MOUSE_CLICKED);
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {
        if (mouseListenerIsActive) {
            if (mouseEvent.getEventType() == MouseEventType.MOUSE_CLICKED) {
                this.menu.showInstructions();
                mouseListenerIsActive = false;
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent var1) {
    }
}
