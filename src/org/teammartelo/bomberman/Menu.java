package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.objects.Bomb;
import org.teammartelo.bomberman.objects.Enemy;
import org.teammartelo.bomberman.sound.BGM;
import org.teammartelo.bomberman.sound.Sound;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class Menu {

    // fields
    private MouseListener1 mouseListener;
    private MouseListener2 mouseListener2;
    private KeyboardListener keyboardListener;
    private Field field;
    private Bomberman player;
    private Enemy enemy;
    public static BGM bgm;
    public static Sound menuMx = new Sound("/resources/bmanSound/mxMenu.wav");
    private Picture pause = new Picture(10, 10, "resources/menu/pause.png");
    public static boolean paused;
    public static boolean canRestart;

    // constructor
    public Menu() {
        canRestart = false;
        paused = false;
        //System.out.println("test");
        menuMx.setLoop(-1);
        Picture mainMenu = new Picture(Field.PADDING2, Field.PADDING2,
                "resources/menu/startmenu.png");
        mainMenu.draw();
        mouseListener = new MouseListener1(this);
        //menuMx.play(true);
    }

    public void showInstructions() {
        mouseListener2 = new MouseListener2(this);
        Picture instructions = new Picture(Field.PADDING2, Field.PADDING2,
                "resources/menu/instructions.png");
        instructions.draw();
    }

    public void showGame() {
        field = new Field();
        player = new Bomberman();
        bgm = new BGM();
        enemy = new Enemy(player);
        enemy.startEnemy();
        keyboardListener = new KeyboardListener(player, bgm,this);
    }

    public void setEnemy() {
        enemy = null;
    }

    public void pause() {
        if(!Bomb.stopMotherFucker) {

            if (!paused) {
                pause.draw();
                paused = true;
                bgm.close();
                bgm.playing = false;
            } else if (paused) {
                pause.delete();
                paused = false;
                bgm.mute();
                bgm.playing = true;
            }
        }
    }

    public void restartApplication() throws IOException, URISyntaxException {
        final String javaBin = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
        final File currentJar = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI());

        if(!currentJar.getName().endsWith(".jar"))
            return;

        final ArrayList<String> command = new ArrayList<>();
        command.add(javaBin);
        command.add("-jar");
        command.add(currentJar.getPath());

        final ProcessBuilder builder = new ProcessBuilder(command);
        builder.start();
        System.exit(0);
    }

}
