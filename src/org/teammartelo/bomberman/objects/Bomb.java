package org.teammartelo.bomberman.objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.Bomberman;
import org.teammartelo.bomberman.Field;
import org.teammartelo.bomberman.Menu;
import org.teammartelo.bomberman.interfaces.Explodable;
import org.teammartelo.bomberman.sound.Sound;
import java.util.Timer;
import java.util.TimerTask;

public class Bomb implements Explodable {

    // fields
    private boolean exploded;
    private Picture bomb;
    private Picture fireCenter, fireDown, fireUp, fireRight, fireLeft;
    private int posX;
    private int posY;
    private ExplodableWall explodableWall = new ExplodableWall(posX + Field.CELLSIZE, posY + Field.CELLSIZE);
    private Bomberman player;
    private boolean burning;
    public static boolean canDropBomb;
    public static boolean stopMotherFucker;
    private Sound explode = new Sound("/resources/bmanSound/sfxExplode.wav");
    private Sound sfxWin = new Sound("/resources/bmanSound/sfxWin.wav");

    // constructor
    public Bomb(int posX, int posY, Bomberman player) {
        exploded = false;
        this.posX = posX;
        this.posY = posY;
        bomb = new Picture(posX, posY, "resources/objects/bomb.png");
        bomb.draw();
        bomb.grow(-5, -5);
        this.explode();
        this.player = player;
        burning = false;
        canDropBomb = true;
        stopMotherFucker = false;
    }

    @Override
    public boolean isExploded() {
        return exploded;
    }

    /**
     * timer
     * show fire pictures + timer
     * delete
     */
    @Override
    public void explode() {
        if (!stopMotherFucker) {
            Timer timer = new Timer();
            canDropBomb = false;
            TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    if (!stopMotherFucker) {
                        if(!Menu.paused) {
                            canDropBomb = true;
                            explode.play(true);
                            bomb.delete();
                            exploded = true;
                            fireCenter = new Picture(posX, posY, "resources/objects/flame.png");
                            fireCenter.draw();
                            burning = true;
                            fireExpand();
                            player.getPlayer().delete();
                            player.getPlayer().draw();
                            explodableWall.explode();
                            fireCheck();
                            while (burning) {
                                ezWin();
                                burnBitch();
                            }
                        } else {
                            explode();
                        }
                    }
                }
            };
            timer.schedule(task, 2000);
            extinguishFire();
            burning = false;
        }
    }

    public void extinguishFire() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (!stopMotherFucker) {
                    if (!Menu.paused) {

                            canDropBomb = false;

                        if (fireCenter != null) {
                            fireCenter.delete();
                            fireCenter = null;
                            burning = false;
                        }

                            if (fireDown != null) {
                                fireDown.delete();
                                fireDown = null;
                                burning = false;
                            }
                            if (fireUp != null) {
                                fireUp.delete();
                                fireUp = null;
                                burning = false;
                            }
                            if (fireRight != null) {
                                fireRight.delete();
                                fireRight = null;
                                burning = false;
                            }
                            if (fireLeft != null) {
                                fireLeft.delete();
                                fireLeft = null;
                                burning = false;
                            }
                        }
                    }
                }
        };
        explodableWall.setExploded(true);
        timer.schedule(task, 3000);
    }

    public void fireCheck() {
        for (int j = 0; j < Field.explodableWallField.size(); j++) {
            // BAIXO !!!!!
            if (posX == Field.explodableWallField.get(j).getPosX() &&
                    posY + Field.CELLSIZE - 1 == Field.explodableWallField.get(j).getPosY()) {

                Field.explodableWallField.get(j).explode();
                Field.explodableWallField.remove(j);
                break;
            }
        }

        // CIMA!!!!!
        for (int j = 0; j < Field.explodableWallField.size(); j++) {
            if (posX == Field.explodableWallField.get(j).getPosX() &&
                    posY - Field.CELLSIZE - 1 == Field.explodableWallField.get(j).getPosY()) {


                Field.explodableWallField.get(j).explode();
                Field.explodableWallField.remove(j);
                break;
            }
        }

        // ESQUERDA !!!
        for (int j = 0; j < Field.explodableWallField.size(); j++) {
            if (posX - Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                    posY - 1 == Field.explodableWallField.get(j).getPosY()) {

                Field.explodableWallField.get(j).explode();
                Field.explodableWallField.remove(j);
                break;
            }
        }

        // DIREITA!!!!
        for (int j = 0; j < Field.explodableWallField.size(); j++) {
            if (posX + Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                    posY - 1 == Field.explodableWallField.get(j).getPosY()) {

                Field.explodableWallField.get(j).explode();
                Field.explodableWallField.remove(j);
                break;
            }
        }
    }

    public void fireExpandDown() {

        for (int i = 0; i < Field.pathWallField.length; i++) {

            if ((posX == Field.pathWallField[i].getPosX() &&
                    (posY + Field.CELLSIZE - 1) == Field.pathWallField[i].getPosY()) || (posY - 1 == Field.PADDING + (Field.CELLSIZE * (Field.MATRIX_SIZE - 2)))) {
                return;
            }
        }
        fireDown = new Picture(posX, posY + Field.CELLSIZE - 1, "resources/objects/flame.png");
        fireDown.draw();
    }

    public void fireExpandUp() {
        for (int i = 0; i < Field.pathWallField.length; i++) {

            if ((posX == Field.pathWallField[i].getPosX() &&
                    (posY - Field.CELLSIZE - 1) == Field.pathWallField[i].getPosY()) || (posY - 1 == Field.PADDING)) {
                return;
            }
        }
        fireUp = new Picture(posX, posY - Field.CELLSIZE - 1, "resources/objects/flame.png");
        fireUp.draw();
    }

    public void fireExpandRight() {

        for (int i = 0; i < Field.pathWallField.length; i++) {

            if ((posX + Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    (posY - 1) == Field.pathWallField[i].getPosY()) || (posX == Field.PADDING + (Field.CELLSIZE * (Field.MATRIX_SIZE - 2)))) {
                return;
            }
        }
        fireRight = new Picture(posX + Field.CELLSIZE, posY - 1, "resources/objects/flame.png");
        fireRight.draw();
    }

    public void fireExpandLeft() {

        for (int i = 0; i < Field.pathWallField.length; i++) {

            if ((posX - Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    (posY - 1) == Field.pathWallField[i].getPosY()) || (posX == Field.PADDING)) {
                return;
            }
        }
        fireLeft = new Picture(posX - Field.CELLSIZE, posY - 1, "resources/objects/flame.png");
        fireLeft.draw();
    }

    public void fireExpand() {
        fireExpandDown();
        fireExpandUp();
        fireExpandRight();
        fireExpandLeft();
    }

    public void burnBitch() {

        // DIREITA DIREITA DIREITA DIREITA
        if (player != null) {
            if (fireRight != null) {
                //  System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireRight.getX() == player.getPlayer().getX() && fireRight.getY() - 43 == player.getPlayer().getY()) {
                    player.getPlayer().delete();
                    player = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/gameover.png");
                    picture.draw();
                    Enemy.sfxLose.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    Menu.canRestart = true;
                }
            }
        }

        // ESQUERDA ESQUERDA ESQUERDA ESQUERDA
        if (player != null) {
            if (fireLeft != null) {
                //  System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireLeft.getX() == player.getPlayer().getX() && fireLeft.getY() - 43 == player.getPlayer().getY()) {
                    player.getPlayer().delete();
                    player = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/gameover.png");
                    picture.draw();
                    Enemy.sfxLose.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    Menu.canRestart = true;
                }
            }
        }

        //CIMA CIMA CIMA CIMA
        if (player != null) {
            if (fireUp != null) {
                //   System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireUp.getX() == player.getPlayer().getX() && fireUp.getY() - 43 == player.getPlayer().getY()) {
                    player.getPlayer().delete();
                    player = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/gameover.png");
                    picture.draw();
                    Enemy.sfxLose.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    Menu.canRestart = true;
                }
            }
        }

        // BAIXO BAIXO BAIXO BAIXO
        if (player != null) {
            if (fireDown != null) {
                //   System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireDown.getX() == player.getPlayer().getX() && fireDown.getY() - 43 == player.getPlayer().getY()) {
                    player.getPlayer().delete();
                    player = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/gameover.png");
                    picture.draw();
                    Enemy.sfxLose.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    Menu.canRestart = true;
                }
            }
        }

        // CENTRO CENTRO CENTRO CENTRO
        if (player != null) {
            if (fireCenter != null) {
                //        System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireCenter.getX() == player.getPlayer().getX() && fireCenter.getY() - 44 == player.getPlayer().getY()) {
                    player.getPlayer().delete();
                    player = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/gameover.png");
                    picture.draw();
                    Enemy.sfxLose.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    Menu.canRestart = true;
                }
            }
        }
    }

    public void ezWin() {

        if (Enemy.enemy != null) {

            // DIREITA DIREITA DIREITA DIREITA
            if (fireRight != null) {
                //  System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireRight.getX() == Enemy.enemy.getX() && fireRight.getY() == Enemy.enemy.getX()) {
                    Enemy.enemy.delete();
                    Enemy.enemy = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/win.png");
                    picture.draw();
                    sfxWin.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    burning = false;
                    Menu.canRestart = true;
                    return;
                }
            }

            // ESQUERDA ESQUERDA ESQUERDA ESQUERDA
            if (fireLeft != null) {
                //  System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireLeft.getX() == Enemy.enemy.getX() && fireLeft.getY() == Enemy.enemy.getY()) {
                    Enemy.enemy.delete();
                    Enemy.enemy = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/win.png");
                    picture.draw();
                    sfxWin.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    burning = false;
                    Menu.canRestart = true;
                    return;
                }
            }

            //CIMA CIMA CIMA CIMA
            if (fireUp != null) {
                //   System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireUp.getX() == Enemy.enemy.getX() && fireUp.getY() == Enemy.enemy.getY()) {
                    Enemy.enemy.delete();
                    Enemy.enemy = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/win.png");
                    picture.draw();
                    sfxWin.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    burning = false;
                    Menu.canRestart = true;
                    return;
                }
            }

            // BAIXO BAIXO BAIXO BAIXO
            if (fireDown != null) {
                //   System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireDown.getX() == Enemy.enemy.getX() && fireDown.getY() == Enemy.enemy.getY()) {
                    Enemy.enemy.delete();
                    Enemy.enemy = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/win.png");
                    picture.draw();
                    sfxWin.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    burning = false;
                    Menu.canRestart = true;
                    return;
                }
            }

            // CENTRO CENTRO CENTRO CENTRO
            if (fireCenter != null) {
                //        System.out.println("Os senhores do java obrigam me a ter esta merda");
                if (fireCenter.getX() == Enemy.enemy.getX() && fireCenter.getY() - 1 == Enemy.enemy.getY()) {
                    Enemy.enemy.delete();
                    Enemy.enemy = null;
                    Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                            "resources/menu/win.png");
                    picture.draw();
                    sfxWin.play(true);
                    Menu.bgm.close();
                    stopMotherFucker = true;
                    burning = false;
                    Menu.canRestart = true;
                }
            }
        }
    }
}
