package org.teammartelo.bomberman.objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.interfaces.Explodable;

public class ExplodableWall implements Explodable {

    // fields
    private boolean exploded;
    private int posX;
    private int posY;
    Picture explodableWall;

    // constructor
    public ExplodableWall(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        explodableWall = new Picture(posX, posY, "resources/field/ExplodableBlock.png");
        exploded = false;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }

    @Override
    public boolean isExploded() {
        return true;
    }

    public void setExploded(boolean exploded) {
        this.exploded = exploded;
    }

    @Override
    public void explode() {
        explodableWall.delete();
    }

    public void draw(){
        explodableWall.draw();
    }

}
