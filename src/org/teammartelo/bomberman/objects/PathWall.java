package org.teammartelo.bomberman.objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;

public class PathWall {

    // fields
    private int posX;
    private int posY;
    private boolean collidable;

    // constructor
    public PathWall(int posX, int posY) {
        this.posX = posX;
        this.posY = posY;
        Picture pathWall = new Picture(posX, posY, "resources/field/SolidBlock.png");
        pathWall.draw();
        collidable = true;
    }

    public int getPosX() {
        return posX;
    }

    public int getPosY() {
        return posY;
    }
}
