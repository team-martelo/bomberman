package org.teammartelo.bomberman.objects;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.Bomberman;
import org.teammartelo.bomberman.Field;
import org.teammartelo.bomberman.Menu;
import org.teammartelo.bomberman.sound.Sound;
import java.util.Timer;
import java.util.TimerTask;

public class Enemy {

    // fields
    private static boolean movable;
    public static Picture enemy;
    public Direction direction;
    private boolean dead = false;
    private Bomberman player;
    public static final Sound sfxLose = new Sound("/resources/bmanSound/sfxLose.wav");

    // constructor
    public Enemy(Bomberman player) {
        movable = true;
        this.player = player;
    }

    public void startEnemy() {
        enemy = new Picture(Field.PADDING + Field.CELLSIZE * (Field.MATRIX_SIZE - 2),
                Field.PADDING + Field.CELLSIZE * (Field.MATRIX_SIZE - 2),
                "resources/enemy/enemy-front.png");
        enemy.draw();
        walk();
        gameOver();
    }

    public void enemyDirection() {

        int randomDirection = (int) (Math.random() * 4);
        Direction tempDir;

        switch (randomDirection) {
            case 0:
                tempDir = Direction.RIGHT;
                break;
            case 1:
                tempDir = Direction.DOWN;
                break;
            case 2:
                tempDir = Direction.LEFT;
                break;
            default:
                tempDir = Direction.UP;
        }

        if (this.direction == tempDir) {
            enemyDirection();
        } else {
            this.direction = tempDir;
        }
    }


    public void walk() {

        if (!Menu.paused) {
            if (enemy != null) {
                if (!Bomb.stopMotherFucker) {
                    enemyDirection();

                    if (this.direction == Direction.RIGHT) {
                        if (canMoveRight(enemy.getX(), enemy.getY())) {
                            enemy.load("resources/enemy/enemy-right.png");
                            enemy.translate(Field.CELLSIZE, 0);
                        }
                    } else if (this.direction == Direction.LEFT) {
                        if (canMoveLeft(enemy.getX(), enemy.getY())) {
                            enemy.load("resources/enemy/enemy-left.png");
                            enemy.translate(-Field.CELLSIZE, 0);
                        }
                    } else if (this.direction == Direction.DOWN) {
                        if (canMoveDown(enemy.getX(), enemy.getY())) {
                            enemy.load("resources/enemy/enemy-front.png");
                            enemy.translate(0, Field.CELLSIZE);
                        }
                    } else if (this.direction == Direction.UP) {
                        if (canMoveUp(enemy.getX(), enemy.getY())) {
                            enemy.load("resources/enemy/enemy-back.png");
                            enemy.translate(0, -Field.CELLSIZE);
                        }
                    }

                    if (!dead) {
                        takeNextStep();
                    } else {
                        return;
                    }
                }
            }
        }
    }

    public void takeNextStep() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                walk();
            }
        };
        timer.schedule(task, 400);
    }

    public boolean canMoveRight(int x, int y) {

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x + Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.RIGHT) {

                movable = false;
                return false;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x + Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.RIGHT) {
                        movable = false;
                        return false;
                    } else {
                        movable = true;
                    }
                }
            }
        }
        if (enemy.getX() + Field.CELLSIZE < Field.PADDING + Field.WIDTH) {
            return true;

        } else {
            return false;
        }
    }

    public boolean canMoveLeft(int x, int y) {
        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x - Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.LEFT) {

                movable = true;
                return false;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x - Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.LEFT) {
                        movable = true;
                        return false;
                    } else {
                        movable = true;
                    }
                }
            }
        }
        if (enemy.getX() - Field.CELLSIZE > Field.PADDING) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canMoveDown(int x, int y) {

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x == Field.pathWallField[i].getPosX() &&
                    y + Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.DOWN) {

                movable = false;
                return false;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x == Field.explodableWallField.get(j).getPosX() &&
                            y + Field.CELLSIZE == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.DOWN) {
                        movable = false;
                        return false;
                    } else {
                        movable = true;
                    }
                }
            }
        }
        if (enemy.getY() + Field.CELLSIZE < Field.PADDING + Field.HEIGHT) {
            return true;
        } else {
            return false;
        }
    }

    public boolean canMoveUp(int x, int y) {

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x == Field.pathWallField[i].getPosX() &&
                    y - Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.UP) {
                movable = false;
                return false;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x == Field.explodableWallField.get(j).getPosX() &&
                            y - Field.CELLSIZE == Field.explodableWallField.get(j).getPosY()) {
                        movable = false;
                        return false;
                    } else {
                        movable = true;
                    }
                }
            }
        }
        if (enemy.getY() - Field.CELLSIZE > Field.PADDING) {
            return true;
        } else {
            return false;
        }
    }

    public void gameOver() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                if (player != null) {
                    if (enemy != null) {
                        if (enemy.getX() == player.anchorX && enemy.getY() + Field.CELLSIZE == player.anchorY) {
                            player.getPlayer().delete();
                            player = null;
                            Picture picture = new Picture(Field.PADDING2, Field.PADDING2,
                                    "resources/menu/rip.png");
                            picture.draw();
                            sfxLose.play(true);
                            Menu.bgm.close();
                            Bomb.stopMotherFucker = true;
                            Menu.canRestart = true;
                        }
                    }
                }
                gameOver();
            }
        };
        timer.schedule(task, 10);
    }


    private enum Direction {
        RIGHT,
        LEFT,
        DOWN,
        UP;

        public Enemy.Direction walkDirection() {

            Enemy.Direction direction = Enemy.Direction.RIGHT;

            switch (direction) {
                case LEFT:
                    direction = Enemy.Direction.LEFT;
                    break;
                case RIGHT:
                    direction = Enemy.Direction.RIGHT;
                    break;
                case DOWN:
                    direction = Enemy.Direction.DOWN;
                    break;
                default:
                    direction = Enemy.Direction.UP;
            }
            return direction;
        }
    }
}
