package org.teammartelo.bomberman;

import org.academiadecodigo.simplegraphics.pictures.Picture;
import org.teammartelo.bomberman.interfaces.Explodable;
import org.teammartelo.bomberman.objects.Bomb;
import org.teammartelo.bomberman.sound.Sound;

public class Bomberman implements Explodable {

    // fields
    private Picture player;
    private static final int PLAYER_WIDTH = 64;
    private static final int PLAYER_HEIGHT = 87;
    public int anchorX = 74;
    public int anchorY = 138;
    public Direction direction;
    public static boolean movable;
    private boolean exploded;
    private final Sound foot = new Sound("/resources/bmanSound/sfxFoot.wav");
    private final Sound bomb = new Sound("/resources/bmanSound/sfxDropBomb.wav");

    // constructor
    public Bomberman() {
        player = new Picture(Field.PADDING, (Field.PADDING - (Field.CELLSIZE - PLAYER_HEIGHT / 4)),
                "resources/bomberman/bmanF.png");
        player.draw();
    }

    // bomberman movements
    public void moveRight() {
        if (!Menu.paused) {
            direction = Direction.RIGHT;
            canMoveRight(anchorX, anchorY);
            player.load("resources/bomberman/bmanR.png");

            if ((anchorX < Field.WIDTH) && movable) {
                player.translate(Field.CELLSIZE, 0);
                anchorX = anchorX + Field.CELLSIZE;
                this.foot.play(true);
            }
            movable = true;
        }
    }

    public void moveLeft() {
        if (!Menu.paused) {
            direction = Direction.LEFT;
            canMoveLeft(anchorX, anchorY);
            player.load("resources/bomberman/bmanL.png");

            if ((anchorX > Field.PADDING) && movable) {
                player.translate(-Field.CELLSIZE, 0);
                anchorX = anchorX - Field.CELLSIZE;
                this.foot.play(true);
            }
            movable = true;
        }
    }

    public void moveDown() {
        if (!Menu.paused) {
            direction = Direction.DOWN;
            canMoveDown(anchorX - Field.CELLSIZE, anchorY - Field.CELLSIZE);
            player.load("resources/bomberman/bmanF.png");

            if ((anchorY < Field.HEIGHT + Field.CELLSIZE) && movable) {
                player.translate(0, Field.CELLSIZE);
                anchorY = anchorY + Field.CELLSIZE;
                this.foot.play(true);
            }
            movable = true;
        }
    }

    public void moveUp() {
        if (!Menu.paused) {
            direction = Direction.UP;
            canMoveUp(anchorX - Field.CELLSIZE, anchorY - Field.CELLSIZE);
            player.load("resources/bomberman/bmanB.png");

            if ((anchorY > Field.PADDING + Field.CELLSIZE) && movable) {
                player.translate(0, -Field.CELLSIZE);
                anchorY = anchorY - Field.CELLSIZE;
                this.foot.play(true);
            }
            movable = true;
        }
    }

    public void canMoveRight(int x, int y) {

        if (Bomb.stopMotherFucker) {
            movable = false;
            return;
        }

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x + Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y - Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.RIGHT) {
                movable = false;
                break;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x + Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y - Field.CELLSIZE == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.RIGHT) {
                        movable = false;
                        break;
                    } else {
                        movable = true;
                    }
                }
            }
        }
    }

    public void canMoveLeft(int x, int y) {

        if (Bomb.stopMotherFucker) {
            movable = false;
            return;
        }
        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x - Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y - Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.LEFT) {
                movable = false;
                break;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x - Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y - Field.CELLSIZE == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.LEFT) {
                        movable = false;
                        break;
                    } else {
                        movable = true;
                    }
                }
            }
        }
    }

    public void canMoveDown(int x, int y) {

        if (Bomb.stopMotherFucker) {
            movable = false;
            return;
        }

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x + Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y + Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.DOWN) {
                movable = false;
                break;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x + Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y + Field.CELLSIZE == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.DOWN) {
                        movable = false;
                        break;
                    } else {
                        movable = true;
                    }
                }
            }
        }
    }

    public void canMoveUp(int x, int y) {

        if (Bomb.stopMotherFucker) {
            movable = false;
            return;
        }

        for (int i = 0; i < Field.pathWallField.length; i++) {
            if (x + Field.CELLSIZE == Field.pathWallField[i].getPosX() &&
                    y - Field.CELLSIZE == Field.pathWallField[i].getPosY() &&
                    this.direction == Direction.UP) {
                movable = false;
                break;
            } else {
                for (int j = 0; j < Field.explodableWallField.size(); j++) {
                    if (x + Field.CELLSIZE == Field.explodableWallField.get(j).getPosX() &&
                            y - Field.CELLSIZE == Field.explodableWallField.get(j).getPosY() &&
                            this.direction == Direction.UP) {
                        movable = false;
                        break;
                    } else {
                        movable = true;
                    }
                }
            }
        }
    }

    public void releaseBomb() {
        if(!Menu.paused) {
            if (!Bomb.stopMotherFucker) {
                if (!Bomb.canDropBomb) {
                    new Bomb(player.getX(), player.getY() + (PLAYER_HEIGHT + PLAYER_HEIGHT / 4 - Field.CELLSIZE), this);
                    bomb.play(true);
                }
                // call next two methods for the player to be above the bomb picture
                player.delete();
                player.draw();
            }
        }
    }

    @Override
    public boolean isExploded() {
        return exploded;
    }

    @Override
    public void explode() {
    }

    // inner enum class to define movements
    private enum Direction {
        RIGHT,
        LEFT,
        DOWN,
        UP;

        public Direction walkDirection() {

            Direction direction = Direction.RIGHT;

            switch (direction) {
                case LEFT:
                    direction = Direction.LEFT;
                    break;
                case RIGHT:
                    direction = Direction.RIGHT;
                    break;
                case DOWN:
                    direction = Direction.DOWN;
                    break;
                default:
                    direction = Direction.UP;
            }
            return direction;
        }
    }

    public Picture getPlayer() {
        return player;
    }
}
